import 'dart:async';
import 'dart:convert';
import 'package:contactApp/models/api.dart';
import 'package:contactApp/models/global.dart';
import 'package:flutter/services.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'listEmploye.dart';

class LoginAuth extends StatefulWidget {
  LoginAuth({Key key}) : super(key: key);

  @override
  _LoginAuthState createState() => _LoginAuthState();
}

class _LoginAuthState extends State<LoginAuth> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isAsync = false;
  //List userData;
  Map<String, String> _user = {};

  register() async {
    setState(() {
      isAsync = true;
    });
    http.post(BaseUrl.loginAuth, body: _user).then((res) async {
      var resJson = json.decode(res.body);

      if (resJson['error'] != null) {
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.setBool("isLogin", false);
        setState(() {
          isAsync = false;
          Fluttertoast.showToast(
              msg: resJson['error'],
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          emailController.clear();
          passwordController.clear();
        });
      } else {
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.setBool("isLogin", true);
        savePref(globalUser.email);
        Fluttertoast.showToast(
            msg: "Login Sukses",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        passwordController.clear();
        emailController.clear();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ListEmployee()));
      }
    });
  }

  void submit() {
    new Future.delayed(new Duration(seconds: 3), () {
      register();
    });
  }

  @override
  void initState() {
    super.initState();
    navigateUser();
  }

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: isAsync,
        progressIndicator: CircularProgressIndicator(),
        opacity: 0.5,
        child: WillPopScope(
          child: Center(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Text(
                    'Login ContactApp',
                    style: TextStyle(fontSize: 22),
                  ),
                  new Padding(padding: EdgeInsets.all(10.0)),
                  new Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                              validator: (value) {
                                if (value.trim().isEmpty) {
                                  return 'email harus diisi';
                                }
                                return null;
                              },
                              controller: emailController,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                labelText: 'Email Anda',
                              ),
                              onChanged: (val) {
                                setState(() {
                                  _user['email'] = val;
                                });
                              }),
                        ),
                        Container(
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            obscureText: true,
                            onChanged: (val) {
                              setState(() {
                                _user['password'] = val;
                              });
                            },
                            validator: (value) {
                              if (value.trim().isEmpty) {
                                return 'password harus diisi';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.visiblePassword,
                            controller: passwordController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              labelText: 'Password Anda',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new FlatButton(
                    color: Colors.blue,
                    onPressed: () {
                      setState(() {
                        if (_formKey.currentState.validate()) {
                          submit();
                        }
                      });
                    },
                    child: Text(
                      'Masuk',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
          onWillPop: exitApp,
        ),
      ),
    );
  }

  Future<bool> exitApp() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Konfirmasi Keluar"),
          content: Text("Apa anda yakin ingin keluar dari aplikasi ini?"),
          actions: <Widget>[
            FlatButton(
              child: Text("Tidak"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Ya"),
              onPressed: () {
                SystemNavigator.pop();
              },
            ),
          ],
        );
      },
    );
    return false;
  }

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool("isLogin") ?? false;
    print("status=$status");
    if (status) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => ListEmployee()));
    } else {
      LoginAuth();
    }
  }

  savePref(
    String email,
  ) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      print("save login");
      preferences.setString("name", email);
    });
  }
}

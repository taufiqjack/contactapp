import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DetailsEmployee extends StatefulWidget {
  List userData;
  int index;
  DetailsEmployee({this.userData, this.index});

  @override
  _DetailsEmployeeState createState() => _DetailsEmployeeState();
}

class _DetailsEmployeeState extends State<DetailsEmployee> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(300.0),
        child: AppBar(
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(padding: EdgeInsets.all(50)),
                  CircleAvatar(
                    backgroundImage: NetworkImage(
                        '${widget.userData[widget.index]['avatar']}'),
                    radius: 70,
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  Text(
                    '${widget.userData[widget.index]['first_name']}',
                    style: TextStyle(fontSize: 30),
                  )
                ],
              ),
            ),
          ),
          actions: [
            new Center(
                child: Row(
              children: [
                new InkWell(
                  child: Icon(Icons.star_border),
                  onTap: () {},
                ),
                Padding(padding: EdgeInsets.only(left: 10)),
                new Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: InkWell(
                    child: Icon(Icons.edit),
                    onTap: () {},
                  ),
                ),
              ],
            )),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(20.0, 5.0, 16.0, 8.0),
        child: Container(
            child: ListView(
          children: [
            Card(
              child: ListTile(
                leading: Icon(Icons.phone),
                title: Text('${widget.userData[widget.index]['last_name']}'),
                subtitle: Text('Nama Belakang'),
                trailing: Icon(Icons.message),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.email),
                title: Text('${widget.userData[widget.index]['email']}'),
                subtitle: Text('Email'),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.share),
                title: Text('Bagikan Kontak'),
                subtitle: Text('Bagikan'),
              ),
            )
          ],
        )),
      ),
    );
  }
}

import 'dart:convert';

import 'package:contactApp/models/api.dart';
import 'package:contactApp/models/global.dart' as global;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'listEmploye.dart';

class CreateEmployee extends StatefulWidget {
  CreateEmployee({Key key}) : super(key: key);

  @override
  _CreateEmployeeState createState() => _CreateEmployeeState();
}

class _CreateEmployeeState extends State<CreateEmployee> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isAsync = false;
  bool visible = false;
  Map data;
  List userData;
  Map<String, String> _user = {};

  final TextEditingController name = TextEditingController();
  final TextEditingController jobTitle = TextEditingController();
  //   global.globalContact.name = name.text;
  //   global.globalContact.job = jobTitle.text;

  createContact() {
    setState(() {
      isAsync = true;
    });
    http.post(BaseUrl.createUsers, body: _user).then((res) {
      var resJson = json.decode(res.body);

      if (resJson['error'] != null) {
        setState(() {
          isAsync = false;
          Fluttertoast.showToast(
              msg: "kesalahan -> " + resJson['error'],
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        });
      } else {
        Fluttertoast.showToast(
            msg: "Data Tersimpan",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ListEmployee()));
      }
    });
  }

  // Future createContact() async {
  //   setState(() {
  //     isAsync = true;
  //     visible = true;
  //   });

  //   global.globalContact.name = name.text;
  //   global.globalContact.job = jobTitle.text;

  //   var response = await http.post(BaseUrl.createUsers, body: _user);
  //   var message = jsonDecode(response.body);

  //   // final response = await http.post(BaseUrl.createUsers, body: {
  //   //   "name": name.text,
  //   //   "job": jobTitle.text,
  //   //   "id": id.text,
  //   //   "createdAt": DateTime.now()
  //   // });

  //   if (response.statusCode == 201) {
  //     showDialog(
  //         context: context,
  //         builder: (BuildContext context) {
  //           return AlertDialog(
  //             title: new Text(message['Sukses!']),
  //             actions: <Widget>[
  //               FlatButton(
  //                 child: new Text('OK'),
  //                 onPressed: () {
  //                   Navigator.of(context).pop();
  //                   Navigator.push(
  //                       context,
  //                       MaterialPageRoute(
  //                           builder: (context) => ListEmployee()));
  //                 },
  //               ),
  //             ],
  //           );
  //         });
  //     print(json.decode(response.body));
  //   } else {
  //     setState(() {
  //       isAsync = false;
  //       visible = false;
  //     });

  //     showDialog(
  //         context: context,
  //         builder: (BuildContext context) {
  //           return AlertDialog(
  //             title: new Text(message['Gagal!']),
  //             actions: <Widget>[
  //               FlatButton(
  //                 child: new Text('OK'),
  //                 onPressed: () {
  //                   Navigator.of(context).pop();
  //                 },
  //               ),
  //             ],
  //           );
  //         });
  //     return jsonDecode(response.body);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tambah Kontak',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        actions: [
          new Center(
            child: InkWell(
              child: Text(
                'Simpan  ',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              onTap: () {
                setState(() {
                  if (_formKey.currentState.validate()) {
                    createContact();
                    print('${global.globalContact.name}');
                  }
                });
              },
            ),
          ),
        ],
      ),
      body: ModalProgressHUD(
        inAsyncCall: isAsync,
        progressIndicator: CircularProgressIndicator(),
        opacity: 0.5,
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              child: Center(
                  child: Container(
                child: Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 50.0, 16.0, 8.0),
                      child: Column(
                        children: [
                          Container(
                              alignment: Alignment.center,
                              child: CircleAvatar(
                                child: Icon(Icons.camera_alt),
                                radius: 50,
                              )),
                          Padding(padding: EdgeInsets.only(top: 20)),
                          TextFormField(
                              controller: name,
                              decoration: InputDecoration(
                                  hintText: 'Nama Anda',
                                  icon: Icon(Icons.person)),
                              validator: (value) {
                                if (value.trim().isEmpty) {
                                  return 'Nama harus diisi';
                                }
                                return null;
                              },
                              onChanged: (val) {
                                setState(() {
                                  _user['name'] = val;
                                });
                              }),
                          Padding(padding: EdgeInsets.only(top: 5.0)),
                          TextFormField(
                              controller: jobTitle,
                              decoration: InputDecoration(
                                  hintText: 'Pekerjaan',
                                  icon: Icon(Icons.work)),
                              validator: (value) {
                                if (value.trim().isEmpty) {
                                  return 'Pekerjaan harus diisi';
                                }
                                return null;
                              },
                              onChanged: (val) {
                                setState(() {
                                  _user['job'] = val;
                                });
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
            ),
          ),
        ),
      ),
    );
  }
}

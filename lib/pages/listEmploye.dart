import 'dart:convert';
import 'package:contactApp/models/api.dart';
import 'package:contactApp/pages/createEmployee.dart';
import 'package:contactApp/pages/detailEmployee.dart';
import 'package:contactApp/pages/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListEmployee extends StatefulWidget {
  final List list;
  ListEmployee({this.list});

  @override
  _ListEmployeeState createState() => _ListEmployeeState();
}

class _ListEmployeeState extends State<ListEmployee> {
  Map data;
  List userData;

  Future getUsers() async {
    http.Response response = await http.get(BaseUrl.listContact);
    data = json.decode(response.body);
    //debugPrint(response.body);
    setState(() {
      userData = data["data"];
    });
    debugPrint(userData.toString());
  }

  @override
  void initState() {
    super.initState();
    getUsers().then((value) {
      setState(() {
        userData.addAll(value);
      });
    });
  }

  Icon actionIcon = new Icon(Icons.search);
  Widget appBarTitle = new Text("Kontakku");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: appBarTitle,
        centerTitle: true,
        actions: [
          new IconButton(
            icon: actionIcon,
            onPressed: () {
              setState(() {
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = new Icon(Icons.close);
                  this.appBarTitle = new TextField(
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                    onChanged: (cari) {
                      cari = cari.toLowerCase();
                      // setState(() {
                      //   userData = userData.where((data) {
                      //     var noteTitle = cari..toLowerCase();
                      //     return noteTitle.contains(cari);
                      //   }).toList();
                      // });
                    },
                    decoration: new InputDecoration(
                        prefixIcon: new Icon(Icons.search, color: Colors.white),
                        hintText: "Cari...",
                        hintStyle:
                            new TextStyle(color: Colors.white, fontSize: 16)),
                  );
                } else {
                  this.actionIcon = new Icon(Icons.search);
                  this.appBarTitle = new Text("Kontakku");
                }
              });
            },
          )
        ],
      ),
      floatingActionButton: FabCircularMenu(
          ringDiameter: 300.0,
          ringWidth: 80.0,
          fabColor: Colors.blueAccent,
          fabOpenColor: Colors.white,
          fabOpenIcon: Icon(
            Icons.menu,
            color: Colors.white,
          ),
          children: <Widget>[
            CircleAvatar(
              backgroundColor: Colors.white,
              child: IconButton(
                  icon: Icon(
                    Icons.add,
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CreateEmployee()));
                  }),
            ),
            CircleAvatar(
              backgroundColor: Colors.white,
              child: IconButton(
                  icon: Icon(
                    Icons.exit_to_app,
                  ),
                  onPressed: () {
                    logout();
                  }),
            ),
          ]),
      body: WillPopScope(
        child: Padding(
          padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 8.0),
          child: ListView.builder(
              itemCount: userData == null ? 0 : userData.length,
              itemBuilder: (BuildContext context, int index) {
                final x = userData[index];
                return InkWell(
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(x['avatar']),
                    ),
                    title: Text(x['first_name']),
                    subtitle: Text(x['email']),
                    trailing: Icon(Icons.star_border),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailsEmployee(
                                  index: index,
                                  userData: userData,
                                )));
                  },
                );
              }),
        ),
        onWillPop: exitApp,
      ),
    );
  }

  Future<bool> exitApp() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Konfirmasi Keluar"),
          content: Text("Apa anda yakin ingin keluar dari aplikasi ini?"),
          actions: <Widget>[
            FlatButton(
              child: Text("Tidak"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Ya"),
              onPressed: () {
                SystemNavigator.pop();
              },
            ),
          ],
        );
      },
    );
    return false;
  }

  logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences?.clear();
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginAuth()));
  }
}

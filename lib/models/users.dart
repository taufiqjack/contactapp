import 'package:json_annotation/json_annotation.dart';

part 'users.g.dart';

@JsonSerializable()
class Users {
    Users();

    int id;
    String email;
    // ignore: non_constant_identifier_names
    String first_name;
    // ignore: non_constant_identifier_names
    String last_name;
    String avatar;
    
    factory Users.fromJson(Map<String,dynamic> json) => _$UsersFromJson(json);
    Map<String, dynamic> toJson() => _$UsersToJson(this);
}

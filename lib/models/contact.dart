import 'package:json_annotation/json_annotation.dart';

part 'contact.g.dart';

@JsonSerializable()
class Contact {
    Contact();

    String name;
    String job;
    int id;
    String createdAt;
    
    factory Contact.fromJson(Map<String,dynamic> json) => _$ContactFromJson(json);
    Map<String, dynamic> toJson() => _$ContactToJson(this);
}
